# https://joeferner.github.io/2015/07/15/linux-command-line-html-and-awk/
# sudo apt install html-xml-utils w3m
# https://www.w3.org/Tools/HTML-XML-utils/README

URL=$1
SELECTOR=$2

curl -s "${URL}" \
  | hxnormalize -x \
  | hxselect -i $SELECTOR
  | w3m -dump -cols 2000 -T 'text/html'
